package br.com.leodev.newsapi.endpoint;

import br.com.leodev.newsapi.erro.CustomErrorType;
import br.com.leodev.newsapi.erro.ResourceNotFoundException;
import br.com.leodev.newsapi.model.Student;
import br.com.leodev.newsapi.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("students")
public class StudentEndpoint {

    private final StudentRepository dao;
    @Autowired
    public StudentEndpoint(StudentRepository dao) {
        this.dao = dao;
    }

    @RequestMapping (method = RequestMethod.GET)
    public ResponseEntity<?> listAll(){
        return new ResponseEntity(dao.findAll(), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getStudentById(@PathVariable("id") Long id){
        Student student = dao.findOne(id);
        if (student == null){
            throw new ResourceNotFoundException("NOT FOUND for id: " + id);
        }
        return new ResponseEntity(student, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> saveStudent(@RequestBody Student student){
        return new ResponseEntity<>(dao.save(student), HttpStatus.CREATED);
    }

    @DeleteMapping
    public ResponseEntity<?> deleteStudent(@RequestBody Student student){
        dao.delete(student);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<?> updateStudent(@RequestBody Student student){
        return new ResponseEntity<>(dao.save(student),HttpStatus.OK);
    }

    @GetMapping(path = "/findByName/{name}")
    public ResponseEntity<?> findByName(@PathVariable String name){
        return new ResponseEntity<>(dao.findByName(name), HttpStatus.OK);
    }

}
