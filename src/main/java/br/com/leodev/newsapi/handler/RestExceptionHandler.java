package br.com.leodev.newsapi.handler;

import br.com.leodev.newsapi.erro.ResourceNotFoundDetails;
import br.com.leodev.newsapi.erro.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.ResourceAccessException;

import java.util.Date;

@ControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> handleNotFoundExcepetion(ResourceNotFoundException rfnException){
        ResourceNotFoundDetails rfnDetaisl =  ResourceNotFoundDetails.ResourceNotFoundDetailsBuilder
                .newBuilder()
                .timestamp(new Date().getTime())
                .status(HttpStatus.NOT_FOUND.value())
                .title("Resource not found")
                .detail(rfnException.getMessage())
                .developerMessage(rfnException.getClass().getName())
                .build();
        return new ResponseEntity<>(rfnDetaisl, HttpStatus.NOT_FOUND);
    }

}
