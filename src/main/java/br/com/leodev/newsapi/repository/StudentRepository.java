package br.com.leodev.newsapi.repository;

import br.com.leodev.newsapi.model.Student;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


//Student é a entidade que estaremos representando
// LOng é o tipo do id
public interface StudentRepository extends CrudRepository<Student, Long> {
    List<Student> findByName(String name);
}
